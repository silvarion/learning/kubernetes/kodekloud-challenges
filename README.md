# Kodekloud Kubernetes Challenges

This is just a repo to hold YML files and instructions on how to solve the Kubernetes Challenges from KodeKloud.

Currently there are 4 challenges that I highly recommend to give a try and enjoy solving them on your own. If you're stuck, you can come here and take a look.

First, enroll on the [Kodekloud Kubernetes Challenges](https://kodekloud.com/courses/kubernetes-challenges/)

Then you can start with the list:

- [Challenge 1 - The Jekyll Site](https://kodekloud.com/topic/lab-kubernetes-challenge-1/)
  - [Solution](challenge1/solution.md)
- [Challenge 2 - Fix Cluster and File server](https://kodekloud.com/topic/lab-kubernetes-challenge-2/)
  - [Solution](challenge2/solution.md)
- [Challenge 3 - The Voting Service](https://kodekloud.com/topic/lab-kubernetes-challenge-3/)
  - [Solution](challenge3/solution.md)
- [Challenge 4 - The Redis Cluster](https://kodekloud.com/topic/lab-kubernetes-challenge-4/)
  - [Solution](challenge4/solution.md)