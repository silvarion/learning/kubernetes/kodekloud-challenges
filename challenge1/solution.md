# Challenge 1

[Link to challenge](https://kodekloud.com/topic/lab-kubernetes-challenge-1/)

## Add User credentials

    kubectl config set-credentials martin \
    --client-key=/root/martin.key \
    --client-certificate=/root/martin.crt \
    --embed-certs=false
    
    kubectl config set-context developer --cluster=kubernetes --user=martin

## Create Role

    kubectl create role developer-role -n development --verb="*" --resource="services,pods,persistentvolumeclaims"

### Rolebinding

    kubectl create rolebinding developer-rolebinding --role="developer-role" --namespace=development --user=martin

### Context
    kubectl config set-context developer --user=martin --namespace=development --cluster=kubernetes
    kubectl config use-context developer

### Persistent Volume Claim

Describe the Persistent Volume

    kubectl config use-context kubernetes-admin@kubernetes
    kubectl describe persistentvolume jekyll-site
    kubectl config use-context developer

Note the line `StorageClass:      local-storage`

Create a file named `jekyll-site-pvc.yml` with the following content, making `storageClassName` :

```yaml
---
apiVersion: v1
kind: PersistentVolumeClaim
metadata:
  name: jekyll-site
  namespace: development
spec:
  storageClassName: local-storage
  accessModes:
    - ReadWriteMany
  resources:
    requests:
      storage: 1Gi
```

### Jekyll Pod

Create a file named `jekyll-pod.yml` with the following contents:

```yaml
apiVersion: v1
kind: Pod
metadata:
  name: jekyll
  labels:
    run: jekyll
spec:
  volumes:
  - name: site
    persistentVolumeClaim:
      claimName: jekyll-site
  initContainers:
  - name: copy-jekyll-site
    image: 'kodekloud/jekyll'
    command: ["jekyll", "new", "/site"]
    volumeMounts:
    - name: site
      mountPath: /site
  containers:
  - name: jekyll
    image: 'kodekloud/jekyll-serve'
    volumeMounts:
    - name: site
      mountPath: /site
```

### Jekyll Service

Create a file named `jekyll-service.yml` with the following contents:

```yaml
---
apiVersion: v1
kind: Service
metadata:
  name: jekyll
spec:
  selector:
    run: jekyll
  type: NodePort
  ports:
    - protocol: TCP
      port: 8080
      targetPort: 4000
      nodePort: 30097
```
