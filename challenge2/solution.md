# Challenge 2

## Fix Control Plane

To fix the controlplane node:

1. Check `~/.kube/config` file
    - Fix Port from 6433 to 6443
2. Check `/etc/kubernetes/manifests/kube-apiserver`
    - Fix certificate name
    - Restart kubelet
3. Fix CoreDNS
    - `kubectl edit deployment/coredns -n kube-system`
    - Fix image name and version

## Fix Node01

To fix node01, just uncordon it

`kubectl uncordon node01`

## Deploy File Server

### Copy images to /web on node01

A simple SCP will do

`scp /media/* node01:/web/`

### data-pv

Create a file [pv.yml](pv.yml) with the following contents

```yaml
---
apiVersion: v1
kind: PersistentVolume
metadata:
  name: data-pv
  labels:
    type: local
spec:
  storageClassName: manual
  capacity:
    storage: 1Gi
  accessModes:
    - ReadWriteMany
  hostPath:
    path: "/web"
```

Apply/Create with `kubectl apply -f <filename>`

### data-pvc

Create a file [pvc.yml](pvc.yml) with the following contents

```yaml
---
apiVersion: v1
kind: PersistentVolumeClaim
metadata:
  name: data-pvc
spec:
  storageClassName: manual
  accessModes:
    - ReadWriteMany
  resources:
    requests:
      storage: 1Gi
  volumeName: data-pv
```

Apply/Create as usual with `kubectl apply -f <filename>`

### gop-file-server-pod

Create a file [pod.yml](pod.yml) with the following contents:

```yaml
---
apiVersion: v1
kind: Pod
metadata:
  name: gop-file-server
spec:
  containers:
    - name: gop-file-server
      image: kodekloud/fileserver
      volumeMounts:
        - name: data-store
          mountPath: /web
  volumes:
    - name: data-store
      persistentVolumeClaim:
        claimName: data-pvc
```

Apply/Create as usual with `kubectl apply -f <filename>`

### gop-file-server-service

```yaml
apiVersion: v1
kind: Pod
metadata:
  name: gop-file-server
spec:
  containers:
    - name: gop-file-server
      image: kodekloud/fileserver
      volumeMounts:
        - name: data-store
          mountPath: /web
  volumes:
    - name: data-store
      persistentVolumeClaim:
        claimName: data-pvc
```
