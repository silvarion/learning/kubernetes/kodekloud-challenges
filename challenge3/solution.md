# Challenge 3

## Create namespace vote

    kubectl create namespace vote

### Create vote-deployment

```yaml
---
apiVersion: apps/v1
kind: Deployment
metadata:
  name: vote-deployment
spec:
  replicas: 1
  selector:
    matchLabels:
      app: votingapp
  template:
    metadata:
      labels:
        app: votingapp
    spec:
      containers:
      - name: vote-deployment
        image: kodekloud/examplevotingapp_vote:before
        ports:
        - containerPort: 80
```

Apply the deployment to the vote namespace

    kubectl apply -f vote-deployment.yml -n vote

### Create vote-service

```yaml
---
apiVersion: v1
kind: Service
metadata:
  name: vote-service
spec:
  selector:
    app: votingapp
  type: NodePort
  ports:
    - protocol: TCP
      port: 5000
      targetPort: 80  
      nodePort: 31000
```

## Create result-deployment and result-service

```yaml
---
apiVersion: v1
kind: Service
metadata:
  name: result-service
spec:
  selector:
    app: resultapp
type: NodePort
  ports:
    - protocol: TCP
      port: 5001
      targetPort: 80
      nodePort: 31001
```

```yaml
---
apiVersion: apps/v1
kind: Deployment
metadata:
  name: result-deployment
spec:
  replicas: 1
  selector:
    matchLabels:
      app: resultapp
  template:
    metadata:
      labels:
        app: resultapp
    spec:
      containers:
      - name: result-deployment
        image: kodekloud/examplevotingapp_result:before
        ports:
        - containerPort: 80
```

### Redis-deployment

```yaml
---
apiVersion: apps/v1
kind: Deployment
metadata:
  name: redis-deployment
spec:
  replicas: 1
  selector:
    matchLabels:
      app: redisapp
  template:
    metadata:
      labels:
        app: redisapp
    spec:
      containers:
      - name: redis-deployment
        image: redis:alpine
        ports:
        - containerPort: 6379
        volumeMounts:
          - name: redis-data
            mountPath: /data
      volumes:
      - name: redis-data
        emptyDir: {}
```

## Redis-service

```yaml
---
apiVersion: v1
kind: Service
metadata:
  name: redis
spec:
  selector:
    app: redisapp
  type: ClusterIP
  ports:
    - protocol: TCP
      port: 6379
      targetPort: 6379

```

## DB deployment

```yaml
---
apiVersion: apps/v1
kind: Deployment
metadata:
  name: db-deployment
spec:
  replicas: 1
  selector:
    matchLabels:
      app: dbapp
  template:
    metadata:
      labels:
        app: dbapp
    spec:
      containers:
      - name: db-deployment
        image: postgres:9.4
        env:
        - name: POSTGRES_HOST_AUTH_METHOD
          value: trust
        ports:
        - containerPort: 5432
        volumeMounts:
          - name: db-data
            mountPath: /var/lib/postgresql/data
      volumes:
      - name: db-data
        emptyDir: {}
```

## DB Service

```yaml
---
apiVersion: v1
kind: Service
metadata:
  name: db
spec:
  selector:
    app: dbapp
  type: ClusterIP
  ports:
    - protocol: TCP
      port: 5432
      targetPort: 5432
```

## Worker deployment

```yaml
---
apiVersion: apps/v1
kind: Deployment
metadata:
  name: worker
spec:
  replicas: 1
  selector:
    matchLabels:
      app: worker
  template:
    metadata:
      labels:
        app: worker
    spec:
      containers:
      - name: worker
        image: kodekloud/examplevotingapp_worker
```
