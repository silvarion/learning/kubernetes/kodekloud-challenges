# Challenge 4

This challenge is about creating a Redis Cluster with a Stateful Set

## Persistent Volumes

Create a file [`redis-pvs.yml`](redis-pvs.yml) with the following contents

```yml
---
apiVersion: v1
kind: PersistentVolume
metadata:
  name: redis01
  labels:
    type: local
spec:
  # storageClassName: manual
  capacity:
    storage: 1Gi
  accessModes:
    - ReadWriteOnce
  hostPath:
    path: "/redis01"
---
apiVersion: v1
kind: PersistentVolume
metadata:
  name: redis02
  labels:
    type: local
spec:
  # storageClassName: manual
  capacity:
    storage: 1Gi
  accessModes:
    - ReadWriteOnce
  hostPath:
    path: "/redis02"
---
apiVersion: v1
kind: PersistentVolume
metadata:
  name: redis03
  labels:
    type: local
spec:
  # storageClassName: manual
  capacity:
    storage: 1Gi
  accessModes:
    - ReadWriteOnce
  hostPath:
    path: "/redis03"
---
apiVersion: v1
kind: PersistentVolume
metadata:
  name: redis04
  labels:
    type: local
spec:
  capacity:
    storage: 1Gi
  accessModes:
    - ReadWriteOnce
  hostPath:
    path: "/redis04"
---
apiVersion: v1
kind: PersistentVolume
metadata:
  name: redis05
  labels:
    type: local
spec:
  capacity:
    storage: 1Gi
  accessModes:
    - ReadWriteOnce
  hostPath:
    path: "/redis05"
---
apiVersion: v1
kind: PersistentVolume
metadata:
  name: redis06
  labels:
    type: local
spec:
  capacity:
    storage: 1Gi
  accessModes:
    - ReadWriteOnce
  hostPath:
    path: "/redis06"

```

Apply the file with the following command:

```shell
kubectl apply -f redis-pvs.yml
```
